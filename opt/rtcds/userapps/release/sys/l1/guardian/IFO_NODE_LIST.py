# -*- mode: python; tab-width: 4 -*-
#
# LLO Guardian top node node list

##################################################

import subprocess

EXCLUDE_NODES = [
    'DIAG_MAIN',
    'INITIAL_ALIGNMENT',
    'IFO_OPERATOR',
    'ALIGN_IRXARM',
    'ALIGN_XARM',
    'ALIGN_XARM_TST',
    'ALIGN_YARM',
    'ALIGN_SRX',
    'ALIGN_PRX',
    'ALIGN_BS',
    'DIAG_L0',
    'DIAG_INF',
    'GRD_TST',
    'GRD_MNG',
    'INJ_TRANS',
    'IFO_DRMI',
    'IFO_ARMS',
    'OMC_LOCK',
    'IMC_AUTOLOCK',
    'TCS_ITMX',
    'TCS_ITMY',
    'SQZ_MANAGER',
    'SQZ_LO',
    'SQZ_OPO',
    'SQZ_TTFSS',
    'SQZ_SHG',
    'SQZ_CLF',
    'SQZ_TRANSITIONS',
    'SQZ_DIAG',
    'TIDAL_X',
    'SUS_PI',
    'SUS_1STVIOLIN',
    'SUS_2NDVIOLIN',
    'SUS_3RDVIOLIN',
    'ISI_ITMX_ST1_BLND',
    'ISI_ITMY_ST1_BLND',
    'ISI_ETMX_ST1_BLND',
    'ISI_ETMY_ST1_BLND',
    'ISI_BS_ST1_BLND',
    'BOUNCE_ROLL_DAMP',
    'BRSEY_STAT', 
    'BRS_TEMP_CONTROL',
    'TCS_KAL',
    'SEI_EQ',
    'BRSEY_STAT',
    'BRSEX_STAT',
    'BRSIY_STAT',
    'BRSIX_STAT',
    'BRS_UTILS_ITMX',
    'BRS_UTILS_ITMY',
    'BRS_UTILS_ETMX',
    'BRS_UTILS_ETMY',
    'DIAG_COH',
    'OPS_MODE',
    'SUS_FC2',
    'SEI_HAM8',
    'ISI_HAM8',
    'PEM_MAG_INJ',
    #'HIGH_FREQ_LINES',
    'SLOTHS'
]

#TODO: Revise the above node exclusion list later


def get_nominal_nodes():
    all_nodes = subprocess.check_output(['guardctrl', 'list'], universal_newlines=True).split()
    exclude_nodes = ['IFO'] + EXCLUDE_NODES
    # also exclude SEI Config nodes
    conf_nodes = [conf for conf in all_nodes if 'CONF' in conf]
    exclude_nodes += conf_nodes
    return list(set(all_nodes) - set(exclude_nodes))


IFO_NODE_LIST = get_nominal_nodes()


if __name__ == '__main__':
    for node in get_nominal_nodes():
        print(node)
